﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CaixaBehaviour : NetworkBehaviour
{
    [SerializeField] private GameObject[] _boxPdcs;
    [SerializeField] private float destroyTime = 70f;


    private void Start()
    {
        Invoke("CmdDestroyBox", destroyTime);

    }

    private void OnTriggerEnter(Collider collision)
    {

        CmdDestroyBox();

    }



    [Command]
    public void CmdDestroyBox()
    {        
        RpcDestroyBox();
    }
    
    [ClientRpc]
    public void RpcDestroyBox()
    {
        OnDestroyBox();
    }


    public void OnDestroyBox() {        

        foreach (GameObject _childBox in _boxPdcs)
        {                   
            _childBox.SetActive(true);
        }
        this.gameObject.SetActive(false);
        Destroy(this.gameObject, destroyTime);
        
    }
}
