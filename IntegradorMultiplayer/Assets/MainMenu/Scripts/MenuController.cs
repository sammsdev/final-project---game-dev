﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MenuController : MonoBehaviour
{
    [SerializeField] GameObject _charHolder;
    [SerializeField] GameObject[] _charOption;
   // [SerializeField] GameObject _sceneHolder;
   // [SerializeField] GameObject[] _sceneOption;
    [SerializeField] MenuData menuData;
    private int _qtdChar;
    private int _index = 0;
    private int _qtdScene;
    private int _indexScene = 0;

    void Start()
    {

        _qtdChar = _charHolder.transform.childCount - 1;      
        DeactivateGameObjects(_charOption, _qtdChar);
        _charOption[0].SetActive(true);

        /*_qtdScene = _sceneHolder.transform.childCount - 1;
        DeactivateGameObjects(_sceneOption, _qtdScene);
        _sceneOption[0].SetActive(true); */

    }

  

    public void OnLeftClickChar()
    {
        DeactivateGameObjects(_charOption, _qtdChar);

        _index--;
        if(_index < 0)        
            _index = _qtdChar;

        _charOption[_index].SetActive(true);
    }

    public void OnRightClickChar()
    {
        DeactivateGameObjects(_charOption, _qtdChar);
        _index++;

        if (_index > _qtdChar)
            _index = 0;

        _charOption[_index].SetActive(true);
    }

    public int GetIndexChar()
    {
        return _index;
    }

    /*

    public void OnLeftClickScene()
    {
        DeactivateGameObjects(_sceneOption, _qtdScene);
        _indexScene--;
        if (_indexScene < 0)
            _indexScene = _qtdScene;

        _sceneOption[_indexScene].SetActive(true);
    }

    public void OnRightClickScene()
    {        
        DeactivateGameObjects(_sceneOption, _qtdScene);

        _indexScene++;
        if (_indexScene > _qtdScene)
            _indexScene = 0;
        _sceneOption[_indexScene].SetActive(true);
    }*/

    public void OnClickStart()
    {
        menuData.index = _index;
        SceneManager.LoadScene("Lobby");
    }

  

    public int GetIndexScene()
    {
        return _indexScene;
    }

    public void DeactivateGameObjects(GameObject[] g, int q)
    {
        for (int c = 0; c <= q; c++)
            g[c].SetActive(false);
    }
}
