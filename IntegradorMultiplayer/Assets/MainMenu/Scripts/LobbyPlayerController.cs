﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LobbyPlayerController : NetworkBehaviour
{    
    [SerializeField] MenuData c;
    
    public void SeturpLocalPlayer()
    {
        AvatarPicker(c.index);
    }

    void AvatarPicker(int index)
    {        
        if (isServer)
            RpcAvatarPicked(index);
        else
            CmdAvatarPicked(index);
    }

    [ClientRpc]
    public void RpcAvatarPicked(int avIndex)
    {
        CmdAvatarPicked(avIndex);
    }
    [Command]
    public void CmdAvatarPicked(int avIndex)
    {
        //trocar o nome HUDCustom de acordo com o nome do script do objeto NetworkLobbyManager
        CustomLobby.s_Singleton.SetPlayerTypeLobby(GetComponent<NetworkIdentity>().connectionToClient, avIndex);
    }
    // Use this for initialization
    void Start()
    {
        SeturpLocalPlayer();
    }
}
