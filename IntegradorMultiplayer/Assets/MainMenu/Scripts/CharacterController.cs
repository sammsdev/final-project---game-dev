﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterController : MonoBehaviour
{
    
    private int _playerIndex = 0;
    private string _prefabPlayer;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

 
    public void SetPlayerSelected(int index)
    {
        this._playerIndex = index;
        StartGame();
    }

    public int GetPlayerSelected()
    {
        return this._playerIndex;
    }


    private void StartGame()
    {     

    }

}
