﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;


public class CustomLobby : NetworkLobbyManager
{    
    Dictionary<int, int> currentPlayers;
    static public CustomLobby s_Singleton;

    private void Start()
    {
        s_Singleton = this;
        currentPlayers = new Dictionary<int, int>();
    }


    public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
    {
        if (!currentPlayers.ContainsKey(conn.connectionId)) 
            currentPlayers.Add(conn.connectionId, 0);

        return base.OnLobbyServerCreateLobbyPlayer(conn, playerControllerId);
    }

    public void SetPlayerTypeLobby(NetworkConnection conn, int _type)
    {
        if (currentPlayers.ContainsKey(conn.connectionId))
            currentPlayers[conn.connectionId] = _type;
    }

    public override GameObject OnLobbyServerCreateGamePlayer(NetworkConnection conn, short playerControllerId)
    {
        int index = currentPlayers[conn.connectionId];

        

        GameObject _temp = (GameObject)GameObject.Instantiate(spawnPrefabs[index], Vector3.zero, Quaternion.identity);

        
            

        NetworkServer.AddPlayerForConnection(conn, _temp, playerControllerId);
       

        return _temp;
    }


    public void LoadMenu()
    {
        SceneManager.LoadScene("MainMenu");
        Destroy(this.gameObject);
      
    }

}






















/*
public override GameObject OnLobbyServerCreateGamePlayer(NetworkConnection conn, short playerControllerId) {
    base.OnLobbyServerCreateGamePlayer(conn, playerControllerId);

    GameObject _temp = (GameObject)GameObject.Instantiate(spawnPrefabs[c.index],  startPositions[c.index].position, Quaternion.identity);       
  //  NetworkServer.AddPlayerForConnection(conn, _temp, playerControllerId);



    return _temp;
}

/*
public short playerPrefabIndex;



public class MsgTypes
{
    public const short PlayerPrefab = MsgType.Highest + 1;

    public class PlayerPrefabMsg : MessageBase
    {
        public short controllerID;
        public short prefabIndex;
    }
}


private void Start()
{
    playerPrefabIndex = (short)c.index;
}
public override void OnStartServer()
{
    NetworkServer.RegisterHandler(MsgTypes.PlayerPrefab, OnResponsePrefab);
    base.OnStartServer();
}

public override void OnClientConnect(NetworkConnection conn)
{

    client.RegisterHandler(MsgTypes.PlayerPrefab, OnRequestPrefab);       
    base.OnClientConnect(conn);
}

private void OnRequestPrefab(NetworkMessage netMsg)
{
    MsgTypes.PlayerPrefabMsg msg = new MsgTypes.PlayerPrefabMsg();
    msg.controllerID = netMsg.ReadMessage<MsgTypes.PlayerPrefabMsg>().controllerID;
    msg.prefabIndex = playerPrefabIndex;
    client.Send(MsgTypes.PlayerPrefab, msg);
}


private void OnResponsePrefab(NetworkMessage netMsg)
{
    MsgTypes.PlayerPrefabMsg msg = netMsg.ReadMessage<MsgTypes.PlayerPrefabMsg>();       
    playerPrefab = spawnPrefabs[msg.prefabIndex];
    gamePlayerPrefab = spawnPrefabs[msg.prefabIndex];  
    base.OnServerAddPlayer(netMsg.conn,  msg.controllerID);      

}

public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
{
    MsgTypes.PlayerPrefabMsg msg = new MsgTypes.PlayerPrefabMsg();        
    msg.controllerID = playerControllerId;      
    NetworkServer.SendToClient(conn.connectionId, MsgTypes.PlayerPrefab, msg);
}

private void Update()
{

   playerPrefabIndex = (short)c.index;


} 

}*/



