﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public TextMeshProUGUI timer;
    float currCountdownValue;

    private void Start()
    {
        StartCoroutine(StartCountdown());
    }



    public IEnumerator StartCountdown(float countdownValue = 180)
    {
        currCountdownValue = countdownValue;
        
        while (currCountdownValue > -1)
        {
            Debug.Log("Countdown: " + currCountdownValue);
            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
            timer.text = currCountdownValue.ToString();
            if (currCountdownValue < 30)
                timer.color = Color.red;
            if (currCountdownValue < 1)
                SceneManager.LoadScene("endScene");
        }
    }

}
