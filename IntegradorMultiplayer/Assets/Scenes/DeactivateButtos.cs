﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateButtos : MonoBehaviour
{
    public GameObject[] objects;
   
    public void DeactivateButton()
    {
        foreach(GameObject e in objects)
            e.SetActive(!e.gameObject.activeSelf);
    }

    
}
