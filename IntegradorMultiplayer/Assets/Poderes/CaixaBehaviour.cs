﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CaixaBehaviour : NetworkBehaviour
{
    [SerializeField] private GameObject[] _boxPdcs;
    [SerializeField] private GameObject helice;
    [SerializeField] private GameObject baseH;
    [SerializeField] private float destroyTime = 2f;

    public AudioSource aSorce;
    public AudioClip aClip;

    private void Start()
    {
        Destroy(gameObject, 30f);
    }

    private void OnCollisionEnter(Collision collision)
    {
       if (!aSorce.isPlaying)
            aSorce.PlayOneShot(aClip);
       CmdDestroyBox();          
        
            
    }

  
    [Command]
    public void CmdDestroyBox()
    {        
        RpcDestroyBox();
    }
    
    [ClientRpc]
    public void RpcDestroyBox()
    {
        OnDestroyBox();
    }


    public void OnDestroyBox() {

        
        foreach (GameObject _childBox in _boxPdcs)
        {
            // _childBox.transform.parent = null;
            helice.SetActive(false);
            baseH.SetActive(false);
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<BoxCollider>().enabled = false;
            
            _childBox.SetActive(true);

        }       

        Destroy(this.gameObject, destroyTime);
        
    }

   
}
