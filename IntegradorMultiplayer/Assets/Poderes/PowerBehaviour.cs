﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PowerBehaviour : NetworkBehaviour
{
    GameObject power = null;
    public Vector3 dir = Vector3.zero;
    float speed;
    
    void Start()
    {
        
        speed = 15f;
        Destroy(this.gameObject, 5f);
    }

    private void Update()
    {
        Move();
    }
    public void Move()
    {
        this.transform.Translate(dir * speed * Time.deltaTime, Space.World);
        if (this.transform.position.z != 0)
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0);
        }
    }





}
