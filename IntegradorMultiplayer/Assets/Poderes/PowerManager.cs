﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PowerManager : NetworkBehaviour
{

    private GameObject _boxPref;
    [SerializeField] private float _spawnTimeBox = 10f;
    float currentTime = 0f;

    private void Start()
    {
        _boxPref = NetworkLobbyManager.singleton.spawnPrefabs[5];
        if (isServer)
            CmdSpawnCaixa();

    }



    private IEnumerator WaitToSpaw()
    {

        while (true)
        {
            SpawnCaixa();
            yield return new WaitForSeconds(_spawnTimeBox);

        }
    }

    [Command]
    public void CmdSpawnCaixa()
    {
        RpcSpawnCaixa();
    }
    

    [ClientRpc]
    public void RpcSpawnCaixa()
    {
        if (isServer)
            StartCoroutine(WaitToSpaw());
    }

    private void SpawnCaixa()
    {
        float posX = Random.Range(-12.0f, 15.0f);
        Vector3 spawnPos = new Vector3(posX, 25, 0);
        GameObject box = (GameObject)Instantiate(_boxPref, spawnPos, Quaternion.identity);
        NetworkServer.Spawn(box);

    }


    

    



    
}
