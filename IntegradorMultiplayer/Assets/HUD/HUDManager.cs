﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    public RawImage localPlayerHolder;

    public RawImage[] playerSprites;

    public GameObject[] players = null;

    GameObject localPlayer;
    public TextMeshProUGUI localPlayerScore;
    public TextMeshProUGUI[] playerScores = null;

    public RankManager rank;

    private void Start()
    {
        StartCoroutine("StartHUD");
        localPlayer = GameObject.FindGameObjectWithTag("localplayer");
    }

    IEnumerator StartHUD()
    {
        yield return new WaitForSeconds(0.3f);
        SetHUD();
        StopCoroutine("StartHUD");
    }

    private void SetHUD()
    {
        localPlayer = GameObject.FindGameObjectWithTag("localplayer");
        localPlayerHolder.texture = localPlayer.GetComponent<PlayerController>().avatar;

        localPlayerScore.text = Mathf.Abs(localPlayer.GetComponent<PlayerController>().score).ToString();


        players = GameObject.FindGameObjectsWithTag("player");

        if(players.Length > 0)
        {
            for (int i = 0; i < players.Length; i++)
            {
                playerSprites[i].texture = players[i].GetComponent<PlayerController>().avatar;
                playerScores[i].text = Mathf.Abs(players[i].GetComponent<PlayerController>().score).ToString();

            }
               
            

        } else
        {
            playerSprites[0].enabled = false;
            playerSprites[1].enabled = false;           

        }

    }


    private void Update()
    {
        localPlayerScore.text = Mathf.Abs(localPlayer.GetComponent<PlayerController>().score).ToString();

        for (int i = 0; i < players.Length; i++)
        {
            playerScores[i].text = Mathf.Abs(players[i].GetComponent<PlayerController>().score).ToString();
        }

    }
}
