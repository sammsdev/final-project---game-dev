﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;



public class PowerController : NetworkBehaviour
{
    public AudioSource aSorce;
    public AudioClip clip;
    private int powerIndex = -1;
    public Transform Origin;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && isLocalPlayer)
        {
            
            if(powerIndex != -1)
            {
                if (!aSorce.isPlaying)
                    aSorce.PlayOneShot(clip);
                if (isServer)                
                    RpcSpawnPower();                    
                else                
                    CmdSpawnPower();               
            }
        }
    }
    
    [Command]
    private void CmdSpawnPower()
    {
        RpcSpawnPower();
    }

    [ClientRpc]
    private void RpcSpawnPower()
    {
        SpawnObj();
    }

    public void SpawnObj()
    {
        Vector3 dir = GetComponent<HookController>().character.GetComponent<Transform>().transform.forward;
        GameObject p = Instantiate((GameObject)NetworkLobbyManager.singleton.spawnPrefabs[powerIndex], new Vector3(Origin.position.x, Origin.position.y, 0), Quaternion.identity);
        p.GetComponent<PowerBehaviour>().dir = dir;

        if (isServer)
            RpcSyncPowerIndex(-1);
        else
            CmdSyncPowerIndex(-1);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("caixa"))
        {  
           var randomIndex = GenerateRandomIndex();            
            CmdSyncPowerIndex(randomIndex);
        }

    }


    private int GenerateRandomIndex()
    {
        return 6;
    }

    #region SyncPower
    private void CmdSyncPowerIndex(int i)
    {
        
        RpcSyncPowerIndex(i);
    }

    private void RpcSyncPowerIndex(int i)
    {
        powerIndex = i;
    
    }
    #endregion
}
