﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class PlayerController : NetworkBehaviour
{
    private float _minDistanceToPoint = 2f;
    public bool isConnect;
    private GameObject[] points;
    private Animator _playerAnimator;
    private Transform _targetRotation;
    private float _rotationSpeed = 6f;
    private Quaternion _originalRot;
    private Rigidbody rb;
    private HookController hk;
    [SyncVar]
    public int score = 1;
    public Texture avatar;

    private void Start()
    {
        
        OnSpawn();             
        points = GameObject.FindGameObjectsWithTag("contactPoint");
        _playerAnimator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        hk = GetComponent<HookController>();
    }


    private void OnSpawn()
    {       
        float pos = Random.Range(-12.0f, 15.0f);
        transform.position = new Vector3(pos, 60f, 0);
        _originalRot = this.gameObject.transform.rotation;
        score--;
    }


    public Transform FindNearPoint()
    {
        
        if (!isConnect)
        {
           
            points = GameObject.FindGameObjectsWithTag("contactPoint");

            for (int i = 0; i < points.Length; i++)
            {
                for (int j = i; j < points.Length; j++)
                {
                    if (Vector3.Distance(points[i].transform.position, this.transform.position) > Vector3.Distance(points[j].transform.position, this.transform.position))
                    {
                        GameObject near = points[j];
                        points[j] = points[i];
                        points[i] = near;
                    }

                }
            }

            _targetRotation = points[0].transform;
            return _targetRotation;

        }        
        return null;
            
    } 


    public void SetConnection(bool c)
    {
        isConnect = c;

    }

    public void Update()
    {
                
        if (isConnect)
        {
            _playerAnimator.SetBool("isFalling", false);
            _playerAnimator.SetBool("isSwinging", true);
        }

        else
        {
            _playerAnimator.SetBool("isSwinging", false);
            _playerAnimator.SetBool("isFalling", true);
            _playerAnimator.SetFloat("JumpFall", rb.velocity.y);
        }

        RotateToTarget(isConnect);

        if (this.transform.position.y < -15f)
        {
            OnSpawn();
        }
    }


    private void RotateToTarget(bool isConnect)
    {

        if (isConnect)
        {
            if (Vector3.Distance(this.transform.position, _targetRotation.position) < hk.maxDist && Vector3.Distance(this.transform.position, _targetRotation.position) > hk.minDist)
            {
                Vector3 targetDir = _targetRotation.transform.position - this.transform.position;
                float angleRad = Mathf.Atan2(targetDir.y, targetDir.x);
                float angleDeg = angleRad * Mathf.Rad2Deg;
                angleDeg -= hk.GetOffSet();
                Quaternion newRotZ = Quaternion.AngleAxis(angleDeg, Vector3.forward);
                this.transform.rotation = Quaternion.Lerp(this.transform.rotation, newRotZ, _rotationSpeed * Time.deltaTime);
            }

        } else
        {
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.identity, 2f * Time.deltaTime);
        }
            
           
                

            
        
                

    }

}
