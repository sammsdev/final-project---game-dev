﻿
using UnityEngine;
using UnityEngine.Networking;

public class FlipBehaviour : MonoBehaviour
{

    public void Flip(Vector3 point)
    { 
        
        if (this.transform.position.x > point.x)
            this.transform.localEulerAngles = new Vector3(0, 270, 0);
        else
          this.transform.localEulerAngles = new Vector3(0, 90, 0);
      
    }
}
