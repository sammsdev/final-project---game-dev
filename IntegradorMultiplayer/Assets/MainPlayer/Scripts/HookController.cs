﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class HookController : NetworkBehaviour
{
    public LineRenderer _ropeRenderer;
    private Transform _pointToConnect;
    public bool isConnected = false;
    [SerializeField] private Material _ropeMaterial;
    public GameObject character;
    public GameObject rightHand;
    private Rigidbody rb;
    private GameObject[] points;
    private PlayerController player;
    private int dir = 0;
    private int offset = 0;
    private bool canImpulse = false;
    private bool hasConnected = false;
    public float maxDist = 8f;
    public float minDist = 3f;
    [SerializeField] private GameObject[] _indicator;
    public Transform origin;


    public override void OnStartLocalPlayer()
    {
        this.gameObject.tag = "localplayer";



    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        if (_ropeRenderer == null)
            _ropeRenderer = GetComponent<LineRenderer>();
        _ropeRenderer.material = _ropeMaterial;
        if (!isLocalPlayer)
        {
            // _ropeMaterial.color = Color.red;
            foreach (GameObject g in _indicator)
            {
                g.GetComponent<Renderer>().material.color = Color.red;
                g.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.red * 3f);
            }

            _ropeRenderer.material.color = Color.red;
            _ropeRenderer.material.SetColor("_EmissionColor", Color.red * 3f);
            this.gameObject.tag = "player";
        } 

        _ropeRenderer.startWidth = 0.2f;

        player = GetComponent<PlayerController>();
    }

    public void SetJoint()
    {
        if (hasConnected == false)
        {
            _pointToConnect = player.FindNearPoint();

            if (this.transform.position.x > _pointToConnect.position.x)
            {
                dir = -1;
                character.transform.localEulerAngles = new Vector3(0, 270, 0);
                offset = 110;
            }
            else
            {
                character.transform.localEulerAngles = new Vector3(0, 90, 0);
                dir = 1;
                offset = 60;

            }

            hasConnected = true;
        }

    }

    public float GetOffSet()
    {
        return offset;
    }

    private void Update()
    {
        if (isConnected)
        {
            DrawRope();
            
        }

        else
            _ropeRenderer.enabled = false;
    }

    public void DrawRope()
    {
        if (Vector3.Distance(this.transform.position, _pointToConnect.transform.position) < maxDist && Vector3.Distance(this.transform.position, _pointToConnect.transform.position) > minDist && GetComponent<InputManager>().CanConnect)
        {
            CastRay();
            rb.isKinematic = true;
            canImpulse = true;
            _ropeRenderer.enabled = true;


            _ropeRenderer.SetPosition(0, rightHand.transform.position);
            _ropeRenderer.SetPosition(1, _pointToConnect.position);

            this.transform.RotateAround(_pointToConnect.transform.position, Vector3.forward * dir, 300f * Time.deltaTime);
        }

    }

    public void BreakJoint()
    {
        _pointToConnect = null;
        rb.isKinematic = false;

        hasConnected = false;
    }

    public void FixedUpdate()
    {
        if (canImpulse)
        {
            rb.velocity = Vector3.zero;
            rb.AddForce(character.transform.forward * 17f, ForceMode.Impulse);
            canImpulse = false;
        }

    }




    private void CastRay()
    {
        var dir = (_pointToConnect.position - origin.position).normalized;
        var dist = Vector3.Distance(_pointToConnect.position, origin.position);

        Debug.DrawLine(origin.position, _pointToConnect.position, Color.red);
        RaycastHit hit;
        Physics.Raycast(origin.position, dir, out hit, dist);
        
        if(hit.collider != null)
        {           

            if ( hit.transform.CompareTag("power") || hit.transform.CompareTag("player") && isLocalPlayer)
            {
                Debug.Log(hit.transform.tag);
                GetComponent<InputManager>().CanConnect = false;
                StartCoroutine("RestartCanConnect");
            }

        }
    }

    
    IEnumerator RestartCanConnect()
    {        
        yield return new WaitForSeconds(2f);
        GetComponent<InputManager>().CanConnect = true;
       StopCoroutine("RestartCanConnect");
    }


}
