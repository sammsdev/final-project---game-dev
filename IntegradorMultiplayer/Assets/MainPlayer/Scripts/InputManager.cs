﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class InputManager : NetworkBehaviour
{
    
    public HookController hook;

    [SerializeField] private Transform pointToConnect;
    private PlayerController player;
    [SyncVar]
    public bool CanConnect = true;

    private void Start()
    {
        player = GetComponent<PlayerController>();
    }

    void Update()
    {

        Debug.Log(CanConnect);
        if (isLocalPlayer)
        {
            
            if (Input.touchCount > 0 || Input.GetMouseButton(0) && CanConnect)
            {
                if (!player.isConnect)
                {
                    CmdConnectToPoint();
                }

            }
            else
            {
                CmdBreakJoint();
            }
        }
    }


    [Command]
    public void CmdConnectToPoint()
    {
      
        RpcConnectToPoint();
    }

    [ClientRpc]
    public void RpcConnectToPoint()
    {
        hook.isConnected = true;
        hook.SetJoint();
        player.isConnect = true;       
       
    }


    [Command]
    public void CmdBreakJoint()
    {        
        
        RpcBreakJoin();
    }

    [ClientRpc]
    public void RpcBreakJoin()
    {
        hook.BreakJoint();
        player.isConnect = false;
        hook.isConnected = false;

    }

    
}
  