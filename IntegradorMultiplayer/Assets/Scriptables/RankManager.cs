﻿using UnityEngine;

[CreateAssetMenu(fileName = "Rank", menuName = "Rank")]
public class RankManager : ScriptableObject
{
    public int[] scores;
    public Texture[] avatar;


}